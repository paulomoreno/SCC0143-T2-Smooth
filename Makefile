CC = gcc
MPICC = mpicc
MPIRUN = mpirun
CFLAGS = -I. -g
PFLAGS = -fopenmp
OCVFLAGS = `pkg-config --cflags opencv`
LIBS = `pkg-config --libs opencv`

all: serial parallel
	$(CC) $(CFLAGS) $(OCVFLAGS) serial.o -o serial $(LIBS)
	$(MPICC) $(CFLAGS) $(PFLAGS) $(OCVFLAGS) parallel.o -o parallel $(LIBS) 

testWorkload:
	$(MPICC) testWorkload.c -o testWorkload

run: parallel
	$(MPIRUN) -n 10 parallel ${ARGS}

serial:
	$(CC) $(CFLAGS) $(OCVFLAGS) -c serial.c

parallel:
	$(MPICC) $(PFLAGS) $(CFLAGS) $(OCVFLAGS) -c parallel.c

clean:
	rm -rf *.o serial parallel testWorkload
