#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

// OpenCV
#include <cv.h>
#include <highgui.h>

// OpenMP and OpenMPI
#include <omp.h>
#include <mpi.h>

#define RED 2
#define GREEN 1
#define BLUE 0

//Variable used to calculate execution elapsed time
double elapsed;

int is_master = 0;

int calculateSinglePixel(IplImage *img, int i, int j, int channel){

	int height = img->height;
	int width = img->width;
	int step = img->widthStep;
	int channels = img->nChannels;

	int x, y, count = 0;
	int sum = 0;

	for(x=i-2; x<=i+2; x++){ // height
		for(y=j-2; y<=j+2; y++){ // width
			if (!(x<0 || y<0 || x>=height || y>=width)){
				sum += (unsigned char) img->imageData[x*step + y*channels + channel];
				count++;
			}	
		}		
	}
	sum /= count;
	return sum;
}

void smooth(IplImage *img, IplImage *result, int start, int end){
	int i, j, k, pixel;

	int height = img->height;
	int width = img->width;
	int step = img->widthStep;
	int channels = img->nChannels;

	for(i = start; i< end; i++){
		#pragma omp parallel
		{
			#pragma omp for private(k)
			for(j=0; j < width; j++){
				for(k = 0; k < channels; k++){
					result->imageData[i*step + j*channels + k] = calculateSinglePixel(img, i, j, k);	
				}
			}
		}
	}
}

int mainSmooth(IplImage *img)
{
        struct timespec start, finish;
        double elapsed_temp;

	IplImage *result;


	// create a window
	//cvNamedWindow("mainWin", CV_WINDOW_AUTOSIZE); 
	//cvMoveWindow("mainWin", 100, 100);

	// Copying the image
	result = cvCreateImage(cvGetSize(img), img->depth, img->nChannels);

	int numtasks, rank, rc, dest, source, count, tag=1;
	MPI_Status Stat;
	int indexes[2];
	int i = 0, j, k,l;
	char *sendBuffer, *receiveBuffer;
	int last_index;

	char hostname[20];
	int lenght;


	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

	// Getting image properties
	int height = img->height;
	int width = img->width;
	int step = img->widthStep;
	int channels = img->nChannels;

	// Image size in columns
	int size = height;

	// rank 0 is the master node
	if (rank == numtasks - 1) {
		is_master = 1;

		receiveBuffer = (char*) calloc(sizeof(char), height*width*channels);
	
		clock_gettime(CLOCK_MONOTONIC, &start);
		
		int workload = size/numtasks;
		if (workload == 0){
			workload = 1;
		}

		for(i = 0; i < numtasks - 1; i++){
			indexes[0] = i*workload;
			indexes[1] = indexes[0] + workload;

			rc = MPI_Send(indexes, 2, MPI_INT, i, tag, MPI_COMM_WORLD);
		}
		
		indexes[0] = i*workload;
		indexes[1] = indexes[0] + workload;

		// Allocating memory for sendBuffer
		lenght = indexes[1] - indexes[0];
		sendBuffer = (char*) calloc(sizeof(char), lenght*width*channels);

		if(indexes[1] > size || indexes[1] < size) last_index = size;
		else last_index = indexes[1];

		l=0;
		smooth(img, result, indexes[0], last_index);
		for (i=indexes[0]; i<indexes[1]; i++){	
			for(j=0; j < width;j++){
				for(k = 0; k < channels; k++){
					sendBuffer[l] = result->imageData[i*step + j*channels + k];
					l++;
				}
			}
		}

		MPI_Gather(sendBuffer, lenght*width*channels, MPI_CHAR, result->imageData, lenght*width*channels, MPI_CHAR, numtasks - 1, MPI_COMM_WORLD);
		//cvShowImage("mainWin", result);
		
        	clock_gettime(CLOCK_MONOTONIC, &finish);
        	elapsed_temp = (finish.tv_sec - start.tv_sec);
        	elapsed_temp += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
	        elapsed += elapsed_temp;

		//cvWaitKey(0);
		cvSaveImage("smooth_result.jpg", result, 0);

		//result->imageData = receiveBuffer;
		//MPI_Get_processor_name(hostname, &lenght);
		//printf("Process %d from host %s: Calculating from %d to %d\n", rank, hostname, indexes[0], indexes[1]);


	}
	// other ranks are the slave nodes
	else {
		// Receiving data from master
		rc = MPI_Recv(indexes, 2, MPI_INT, numtasks - 1, tag, MPI_COMM_WORLD, &Stat);

		// Allocating memory for sendBuffer
		lenght = indexes[1] - indexes[0];
		sendBuffer = (char*) calloc(sizeof(char), lenght*width*channels);

		// Getting the hostname
		//MPI_Get_processor_name(hostname, &lenght);
		//printf("Process %d from host %s: Calculating from %d to %d\n", rank, hostname, indexes[0], indexes[1]);
	
		l=0;
		smooth(img, result, indexes[0], indexes[1]);
		for (i = indexes[0]; i < indexes[1]; i++){	
			for(j=0; j < width; j++){
				for(k=0; k< channels; k++){
					sendBuffer[l] = result->imageData[i*step + j*channels + k];
					l++;
				}
			}
		}
		MPI_Gather(sendBuffer, lenght*width*channels, MPI_CHAR, NULL, lenght*width*channels,  MPI_CHAR, numtasks - 1, MPI_COMM_WORLD);
	
	}

	cvReleaseImage(&result);

}


int main(int argc, char**argv){
        int i,rank;
	IplImage *img;

	if(argc < 2){
		printf("Usage: main <image-file-name>\n\7");
		exit(0);
	}


        if(argc == 2) {

		// load an image  
		img = cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
		if(!img){
			printf("Could not load image file: %s\n", argv[1]);
			exit(0);
		}

		MPI_Init(&argc, &argv);
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
                
		elapsed = 0;
                for(i = 0; i < 10; i++){
			if(rank == 0)
                        	printf("Iteration %d of 10\n", i + 1); 
                        mainSmooth(img);
                }

		if (is_master){
                	elapsed = (double) elapsed/10;
                	printf("Average time of 10 calculations: %.2lfs\n", elapsed);
		}

		MPI_Finalize();
        }
        else{
                printf("Not enough parameters - Missing filename\n");
        }
        return 0;
}

