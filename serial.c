#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

// OpenCV
#include <cv.h>
#include <highgui.h>

#define RED 2
#define GREEN 1
#define BLUE 0

double elapsed;

int calculateSinglePixel(IplImage *img, int i, int j, int channel){

	int height = img->height;
	int width = img->width;
	int step = img->widthStep;
	int channels = img->nChannels;

	int x, y, count = 0;
	int sum = 0;

	for(x=i-2; x<=i+2; x++){ // height
		for(y=j-2; y<=j+2; y++){ // width
			if (!(x<0 || y<0 || x>=height || y>=width)){
				sum += (unsigned char) img->imageData[x*step + y*channels + channel];
				count++;
			}	
		}		
	}
	sum /= count;
	return sum;
}

void smooth(IplImage *img, IplImage *result){
	int i,j,r,g,b;

 	int height = img->height;
	int width = img->width;
	int step = img->widthStep;
	int channels = img->nChannels;

	for(i=0; i<img->height; i++){
		for(j=0; j<img->width; j++){

			r = calculateSinglePixel(img, i, j, RED); 		
			g = calculateSinglePixel(img, i, j, GREEN); 		
			b = calculateSinglePixel(img, i, j, BLUE); 		

			result->imageData[i*step + j*channels + RED] = r;
			result->imageData[i*step + j*channels + GREEN] = g;
			result->imageData[i*step + j*channels + BLUE] = b;
		}
	}
}

int main(int argc, char **argv){
	IplImage* img = 0, *img2 = 0; 
        struct timespec start, finish;
        double elapsed_temp;

	int i;

	if(argc<2){
		printf("Usage: main <image-file-name>\n\7");
		exit(0);
	}

	// load an image  
	img=cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
	if(!img){
		printf("Could not load image file: %s\n",argv[1]);
		exit(0);
	}

	// copying img1 to img2
	img2 = cvCreateImage(cvGetSize(img), img->depth, img->nChannels);

	elapsed = 0;
	for(i = 0; i < 10; i++){
		printf("Iteration %d of 10\n", i + 1); 
		clock_gettime(CLOCK_MONOTONIC, &start);

		smooth(img, img2);

        	clock_gettime(CLOCK_MONOTONIC, &finish);
        	elapsed_temp = (finish.tv_sec - start.tv_sec);
        	elapsed_temp += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
	        elapsed += elapsed_temp;
	}

	elapsed = (double) elapsed/10;
	printf("Average time of 10 calculations: %.2lfs\n", elapsed);

	cvSaveImage("test.jpg", img2, 0);

	return 0;
}
