#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>

int main(int argc, char *argv[])
{
	IplImage* img = 0, *img2 = 0; 
	int height,width,step,channels;
	uchar *data, *result;
	int i,j,k;

	if(argc<2){
		printf("Usage: main <image-file-name>\n\7");
		exit(0);
	}

	// load an image  
	img=cvLoadImage(argv[1], CV_LOAD_IMAGE_COLOR);
	if(!img){
		printf("Could not load image file: %s\n",argv[1]);
		exit(0);
	}

	img2 = cvCreateImage(cvGetSize(img), img->depth, img->nChannels);

	// get the image data
	height    = img->height;
	width     = img->width;
	step      = img->widthStep;
	channels  = img->nChannels;
	data      = (uchar *)img->imageData;
	printf("Processing a %dx%d image with %d channels\n",height,width,channels); 

	result      = (uchar *)img2->imageData;

	// create a window
	cvNamedWindow("mainWin", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("mainWin", 100, 100);

	// invert the image
	for(i=0;i<height;i++) 
		for(j=0;j<width;j++) 
			for(k=0;k<channels;k++)
				result[i*step+j*channels+k] = data[i*step+j*channels+k];

	// show the image
	cvShowImage("mainWin", img2);
	cvSaveImage("test.jpg", img2, 0);

	// wait for a key
	cvWaitKey(0);

	// release the image
	cvReleaseImage(&img );
	cvReleaseImage(&img2 );
	return 0;
}
